from genericpath import isfile
from helper.file import File
from helper.stringreplacer import StringReplacer
from os import makedirs, getcwd, remove, removedirs, rmdir, replace, system
from os.path import isdir
from argparse import ArgumentParser
import json
import zipfile
import requests, zipfile, io
from pathlib import Path


def main():
    print("[Forge Updater] Main Script Started")


    parser = ArgumentParser()
    parser.add_argument("-n", "--namespace", dest="namespace", help="", default="mynamespace")
    parser.add_argument("-p", "--project", dest="project", help="", default="mycoolmod")
    args = parser.parse_args()

    targetFiles = [
        {
            "id": 0, 
            "name": "build", 
            "extension": ".gradle", 
            "replace": [
                {
                    "search": "examplemod", 
                    "override": args.project.lower()
                },
                {
                    "search": "yourname", 
                    "override": args.namespace.lower()
                },
                {
                    "search": "modid", 
                    "override": args.project.lower()
                }
            ]
        }, 
        {
            "id": 1, 
            "name": "src/main/resources/META-INF/mods", 
            "extension": ".toml", 
            "replace": [
                {
                    "search": "examplemod", 
                    "override": args.project.lower()
                },
                {
                    "search": "yourname", 
                    "override": args.namespace.lower()
                },
                {
                    "search": "modid", 
                    "override": args.project.lower()
                }
            ]
        }, 
        {
            "id": 2, 
            "name": "src/main/resources/pack", 
            "extension": ".mcmeta", 
            "replace": [
                {
                    "search": "examplemod", 
                    "override": args.project.lower()
                },
                {
                    "search": "yourname", 
                    "override": args.namespace.lower()
                },
                {
                    "search": "modid", 
                    "override": args.project.lower()
                }
            ]
        }, 
        {
            "id": 3, 
            "name": "src/main/java/com/"+args.namespace.lower()+"/"+args.project.lower()+"/"+args.project, 
            "extension": ".java", 
            "replace": [
                {
                    "search": "examplemod", 
                    "override": args.project.lower()
                },
                {
                    "search": "ExampleMod", 
                    "override": args.project
                },
                {
                    "search": "yourname", 
                    "override": args.namespace.lower()
                },
                {
                    "search": "modid", 
                    "override": args.project.lower()
                }
            ]
        }, 
    ]

    

    print("[Forge Updater] Request Forge Page")
    r2 = requests.get("https://files.minecraftforge.net/net/minecraftforge/forge/")
    url = r2.text[r2.text.rfind("&url=", 0, r2.text.find(".zip"))+len("&url="):r2.text.find(".zip")+len(".zip")]

    print("[Forge Updater] Download newest .mkd Package as .zip \n>> "+ str(url))

    r = requests.get(url)
    z = zipfile.ZipFile(io.BytesIO(r.content))
    print("[Forge Updater] Extract Zip to ./extract/ folder ")
    z.extractall("./extract/")

    system("ls && cd ..")
    if(not isdir("../gradle/wrapper/")):
        makedirs("../gradle/wrapper/")
    if(not isdir("../src/main/resources/")):
        makedirs("../src/main/resources/")
    if(not isdir("../src/main/java/com/"+args.namespace.lower()+"/"+args.project.lower())):
        makedirs("../src/main/java/com/"+args.namespace.lower()+"/"+args.project.lower())
        
    print("[Forge Updater] Delete Readme Files ")
    remove("./extract/CREDITS.txt")
    remove("./extract/LICENSE.txt")
    remove("./extract/README.txt")
    remove("./extract/changelog.txt")

    print("[Forge Updater] Delete Git Files ")
    remove("./extract/.gitattributes")
    remove("./extract/.gitignore")
    
    print("[Forge Updater] Delete Linux Setup for Gradlew")
    remove("./extract/gradlew")
    
    print("[Forge Updater] Delete ExampleMod ")
    if(isfile("../src/main/java/com/"+args.namespace.lower()+"/"+args.project.lower()+"/"+args.project+".java")) is False:
        Path("./extract/src/main/java/com/example/examplemod/ExampleMod.java").rename("../src/main/java/com/"+args.namespace.lower()+"/"+args.project.lower()+"/"+args.project+".java")
    if(isfile("./extract/src/main/java/com/example/examplemod/ExampleMod.java")):
        remove("./extract/src/main/java/com/example/examplemod/ExampleMod.java")
    removedirs("./extract/src/main/java/com/example/examplemod/")

    
    
    print("[Forge Updater] Move Gradlew Files")
    replace("./extract/gradlew.bat", "../gradlew.bat");
    replace("./extract/build.gradle", "../build.gradle");
    replace("./extract/gradle.properties", "../gradle.properties");
    replace("./extract/gradle/wrapper/gradle-wrapper.jar", "../gradle/wrapper/gradle-wrapper.jar");
    replace("./extract/gradle/wrapper/gradle-wrapper.properties", "../gradle/wrapper/gradle-wrapper.properties");
    removedirs("./extract/gradle/wrapper")
    
    print("[Forge Updater] Move Resources Files")
    if(isfile("../src/main/resources/pack.mcmeta")) is False:
        Path("./extract/src/main/resources/pack.mcmeta").rename("../src/main/resources/pack.mcmeta");
    else:
        remove("./extract/src/main/resources/pack.mcmeta")
    if Path("../src/main/resources/META-INF/mods.toml").is_file() is False:
        Path("./extract/src/main/resources/META-INF/").rename("../src/main/resources/META-INF/")
    else:
        remove("./extract/src/main/resources/META-INF/mods.toml")
        removedirs("./extract/src/main/resources/META-INF/")


    print("[Forge Updater] Remap namespace and modname in all files ")
    for mappedFile in targetFiles:
        targetFile = File("../"+mappedFile["name"], mappedFile["extension"])
        if targetFile.doesExist():
            for replaceItem in mappedFile["replace"]:
                data = targetFile.read()
                data = StringReplacer(data, replaceItem["override"], replaceItem["search"], strict=False)
                targetFile.write(data)
        else:
            break;

    print("[Forge Updater] Execute Gradlew Startup Scripts (vsc and eclipse)")
    put = input("[Forge Updater] Setup and test? [any/no]").lower()
    if put != "n" and put != "no":
        system("cd .. && clear && gradlew clean")
        system("cd .. && clear && gradlew --refresh-dependencies")
        system("cd .. && clear && gradlew genEclipseRuns")
        system("cd .. && clear && gradlew eclipse")
        system("cd .. && clear && gradlew runData")
        system("cd .. && clear && gradlew runClient")
    
    print("[Forge Updater] Main Script Ended")


if __name__ == "__main__":
    main()
